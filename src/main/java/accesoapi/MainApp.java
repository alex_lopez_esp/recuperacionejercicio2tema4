package accesoapi;

import com.google.gson.JsonSyntaxException;

import java.io.IOException;

public class MainApp {
    public static PeliculaDaoImpl peliculaDao=new PeliculaDaoImpl();
    public static void main(String[] args) {
        try {
            System.out.println("\n\nBuscar Pelicula Por ID");
            System.out.println(peliculaDao.findbypk("tt0106062"));
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }

        System.out.println("\n");
        try {
            System.out.println("\n\nBuscar Pelicula Por Nombre Pelicula");
            System.out.println(peliculaDao.findByTitle("Matrix"));
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

        System.out.println("\n");
        try {
             System.out.println("\n\nBuscar Pelicula Por fecha");
             System.out.println(peliculaDao.findByYear("1993"));


        } catch (JsonSyntaxException e) {
             System.out.println("To many Results");
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            System.out.println("\n\nBuscar Pelicula Por Typo" );

            System.out.println(peliculaDao.findByType("movie"));
        } catch (JsonSyntaxException e) {
            System.out.println("To many Results");
        } catch (IOException e) {
            e.printStackTrace();
        }

        try{
            System.out.println("\n\nBuscar Pelicula Por Typo" );
            System.out.println(peliculaDao.findByExample("Matrix","1993","series"));
        } catch (IOException e) {
            e.printStackTrace();
         }


    }
}
