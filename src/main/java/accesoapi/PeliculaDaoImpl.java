package accesoapi;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PeliculaDaoImpl {

    public final Peliculas findbypk(String id)throws IOException{
        Peliculas pelicula=null;
        URL url;
        String lin, salida = "";
        Gson gson;
        HttpURLConnection con;

            url=new URL("https://www.omdbapi.com/?i="+id+"&apikey=8465990");
            con=(HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Accept","application/json");
            if (con.getResponseCode() == HttpURLConnection.HTTP_OK){
                BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));

                while ((lin=br.readLine())!=null){
                    salida=salida.concat(lin);

                }
                gson=new Gson();
                pelicula=gson.fromJson(salida,Peliculas.class);
            }
            con.disconnect();



        return pelicula;
    }
    public final Peliculas findByTitle(String title)throws IOException{
        Peliculas pelicula=null;
        URL url;
        String lin, salida = "";
        Gson gson;
        HttpURLConnection con;

            url=new URL("https://www.omdbapi.com/?i=tt3896198&apikey=8465990&t="+title);
            con=(HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Accept","application/json");
            if (con.getResponseCode() == HttpURLConnection.HTTP_OK){
                BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));

                while ((lin=br.readLine())!=null){
                    salida=salida.concat(lin);

                }
                gson=new Gson();
                pelicula=gson.fromJson(salida,Peliculas.class);
            }
            con.disconnect();


        return pelicula;
    }
    public final List<Peliculas> findByYear(String anyo) throws JsonSyntaxException, IOException {

        URL url;
        String lin, salida = "";
        Gson gson;
        HttpURLConnection con;
        List<Peliculas> listaPeliculas =null;

            url=new URL("https://www.omdbapi.com/?apikey=8465990&y="+anyo+"&s=\"\"");
            con=(HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Accept","application/json");
            if (con.getResponseCode() == HttpURLConnection.HTTP_OK){
                BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));

                while ((lin=br.readLine())!=null){
                    salida=lin;
                }
                gson=new Gson();
                listaPeliculas=new ArrayList<Peliculas>(Arrays.asList(gson.fromJson(salida,Peliculas[].class)));
                con.disconnect();
            }




        return listaPeliculas;

    }
    public final List<Peliculas> findByType(String type) throws IOException {

        Peliculas[] pelicula;
        URL url;
        String lin, salida = "";
        Gson gson;
        HttpURLConnection con;
        List<Peliculas> listaPeliculas =null;

            url=new URL("https://www.omdbapi.com/?apikey=8465990&type="+type+"&s=\"\"");
            con=(HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Accept","application/json");
            if (con.getResponseCode() == HttpURLConnection.HTTP_OK){
                BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));

                while ((lin=br.readLine())!=null){
                    salida=salida.concat(lin);
                }
                gson=new Gson();
                pelicula=gson.fromJson(salida,Peliculas[].class);
                listaPeliculas=new ArrayList<Peliculas>(Arrays.asList(pelicula));
                con.disconnect();
            }




        return listaPeliculas;
    }
    public final List<Peliculas> findByExample(String title,String year,String type) throws IOException {


        Peliculas pelicula = null;
        URL url;
        String lin, salida = "";
        Gson gson;
        HttpURLConnection con;
        List<Peliculas> listaPeliculas =null;
       

            url=new URL("https://www.omdbapi.com/?apikey=8465990&t="+title+"&y="+year+"&type="+type);
            con=(HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Accept","application/json");
            if (con.getResponseCode() == HttpURLConnection.HTTP_OK){
                BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));

                while ((lin=br.readLine())!=null){
                    salida=salida.concat(lin);
                }
                gson=new Gson();
                pelicula=gson.fromJson(salida,Peliculas.class);
                listaPeliculas=new ArrayList<>(Arrays.asList(pelicula));
                con.disconnect();
            }




        return listaPeliculas;
    }
}
