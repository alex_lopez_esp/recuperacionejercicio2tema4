package accesoapi;

import java.util.Arrays;

public class Peliculas {
    private String Title;
    private String Type;
    private String Year;
    private String Genre;
    private String Country;
    private String Language;
    private String imdbRating;
    private String imdbVotes;

    public Peliculas(String title, String type, String year, String genre, String country, String language, String imdbRating, String imdbVotes) {
        this.Title = title;
        this.Type = type;
        this.Year = year;
        this.Genre = genre;
        this.Country = country;
        this.Language = language;
        this.imdbRating = imdbRating;
        this.imdbVotes = imdbVotes;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getYear() {
        return Year;
    }

    public void setYear(String year) {
        Year = year;
    }

    public String getGenre() {
        return Genre;
    }

    public void setGenre(String genre) {
        Genre = genre;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getLanguage() {
        return Language;
    }

    public void setLanguage(String language) {
        Language = language;
    }

    public String getImdbRating() {
        return imdbRating;
    }

    public void setImdbRating(String imdbRating) {
        this.imdbRating = imdbRating;
    }

    public String getImdbVotes() {
        return imdbVotes;
    }

    public void setImdbVotes(String imdbVotes) {
        this.imdbVotes = imdbVotes;
    }

    @Override
    public String toString() {
        return "Peliculas{" +
                "Title='" + Title + '\'' +
                ", Type='" + Type + '\'' +
                ", Year='" + Year + '\'' +
                ", Genre=" +Genre +
                ", Country='" + Country + '\'' +
                ", Language='" + Language + '\'' +
                ", imdbRating='" + imdbRating + '\'' +
                ", imdbVotes='" + imdbVotes + '\'' +
                '}';
    }
}
